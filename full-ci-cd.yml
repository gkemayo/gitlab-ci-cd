default:
    #Instruction permettant de charger les modules (la dernière) permettant d'exécuter les commandes maven.
    image: maven:latest

    #mise en cache des dépendances maven downloadées par le job "image"
    #cache:
     # key:
      #  files:
       #   - settings.xml
      #paths:
       # - .m2/repository
    
    before_script: 
       - echo "...........................$START_JOB_MSG......................"
    after_script:  
       - echo "...........................$END_JOB_MSG......................"


stages:
  - build
  - test
  - deploy
  - notification

variables:
   MVN_CMD: mvn
   MVN_GOALS: clean test
   MR: merge_requests
   PUSH: pushes
   WEB: web
   START_JOB_MSG: ""
   END_JOB_MSG: ""
   MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version"


#Définition d'un job caché qui fera reessayer l'exécution d'un Job si son échec est dû à une erreur système du runner, ou une erreur inconnue.
.retry-action:
   retry:
     max: 1
     when:
       - unknown_failure
       - runner_system_failure
       
 #Définition d'un job caché qui fera s'arrêtera le job en cours (et donc le pipeline) si un nouveau pipeline survient lorsque celui-ci est en cours      
.interruptor:
    interruptible: true                  
   
#Définition du job caché de build
.mvn-build-job-generic:    
   stage: build
   extends: 
      - .retry-action
      - .interruptor
   variables:
     START_JOB_MSG: "Start packaging"
     END_JOB_MSG: "End packaging"
   only:
     refs:
       - master      #Seule la branche master permet de declencher le pipeline
     variables:
       - $PUSH       #Permission de déclencher le pipeline par un git push sur la branche master
       - $MR         #Permission de déclencher le pipeline par une merge request sur la branche master
       - $WEB        #Permission de déclencher le pipeline via le bouton Run de GitLab
   except:
     changes:
       - README.TXT   #Tout changement sur des fichiers README.txt ou README.md ne déclenchera pas le pipeline
       - README.md
   script:
      mvn $MAVEN_CLI_OPTS clean compile -DSkipTests


#Définition du job caché de test
.mvn-test-job-generic:     
   stage: test
   extends: 
      - .retry-action
      - .interruptor
   variables:
      START_JOB_MSG: "Start maven test"
      END_JOB_MSG: "End of maven test"
   script:
      mvn $MAVEN_CLI_OPTS clean test         #Exécution de la commande cible maven test


#Définition du job caché de réalisation du déploiement dans Nexus    
.deploy-job-generic:   
   stage: deploy
   image: maven:3.6.3-jdk-8
   extends: 
      - .retry-action
      - .interruptor
   variables:
      START_JOB_MSG: "Start maven deploy"
      END_JOB_MSG: "End of maven deploy"
   #environnement:                                                             #Seuls les users définis dans l'environnement sont autorisés à démarrer un déploiement
    # name: gkemayo
     #url: XXX
   before_script:                                                              #Ce script installe l'agent open-ssh sur la machine du runner pour pouvoir travailler avec git en mode ssh, où on est s'affranchi de la saisie de login/mdp qui bloquerait le process (car fonctonnement en mode batch). Ceci nécessite néanmoins de paramétrer la clé privée ssh dans les Variables de GitLab UI.
      - echo "...........................$START_JOB_MSG......................"
      - echo "#!/bin/bash"
      - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
      - eval $(ssh-agent -s)
      - ssh-add <(echo "$GIT_SSH_GKEMAYO_PRIV_KEY_GUI_VARIABLE")               #On set la valeur de la clé ssh privée à partir de la variable créée dans gitlab ui
      - git config --global user.email "${GITLAB_USER_NAME}"
      - git config --global user.name "${GITLAB_USER_EMAIL}"
      - mkdir -p ~/.ssh
      - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'    #Désactivation du contrôle du ~/.ssh/known_hosts
   script:                                                                      
      - echo "#!/bin/bash"
      - git config --global commit.gpgsign false                               #On desactive la conf GPG qui bloquera le pipeline pour demander le passphrase
      - git checkout -B "$CI_BUILD_REF_NAME"
      - mkdir -p repsettings
      - echo "$SETTINGS_XML_GKEMAYO_GUI_VARIABLE" > repsettings/settings.xml           #On crée le fichier repsettings/settings.xml à partir de la variable créée dans gitlab ui 
      - mvn -s repsettings/settings.xml $MAVEN_CLI_OPTS release:clean release:prepare release:perform deploy -DskipTests=true -DscmCommentPrefix="[skip ci] [maven-release-plugin]" -DreleaseVersion=$RELEASE_VERSION -DdevelopmentVersion=$SNAPSHOT_VERSION ; 
   after_script:
      - echo "#!/bin/bash"
      - git config --global commit.gpgsign true                                #On reactive la conf GPG 
      - echo "...........................$END_JOB_MSG......................"
      

#Définition du job caché de notification en cas de succès     
.notif-success:    
   stage: notification
   when: on_success
   script:
     curl -s --user "api:$MAILGUN_GKEMAYO_API_KEY" 
       "https://api.mailgun.net/v3/$MAILGUN_GKEMAYO_DOMAIN/messages"
       -F from='Gitlab <gitlab@gkemayo.com>'
       -F to=$GITLAB_USER_EMAIL
       -F subject='Perfect! Build has passed'
       -F text='The Build ended successfully. Thanks for you contribution. '


#Définition du job caché de notification en cas d'échec
.notif-failed:    
   stage: notification
   when: on_failure
   script:
     curl -s --user "api:$MAILGUN_GKEMAYO_API_KEY" 
       "https://api.mailgun.net/v3/$MAILGUN_GKEMAYO_DOMAIN/messages"
       -F from='Gitlab <gitlab@gkemayo.com>'
       -F to=$GITLAB_USER_EMAIL
       -F subject='Alert! Build has failed'
       -F text='The Build ended unsuccessfully. Please clic on this link or see the attachement reports to see what happen. Thanks for fixing. '
       -F attachment='@reports/$CI_PROJECT_NAME_junit_report.xml'
  

#Job de Build      
mvn-build-job:
   extends: .mvn-build-job-generic
   allow_failure: false                 #Si ce job échoue, alors on ne continue pas le reste du pipeline
   when: manual                         #Le Pepiline à travers ce job sera déclenché automatiquement
   artifacts:
     expire_in: 1 day
     when: on_success
     name: $CI_PROJECT_NAME-build-artifacts              #Nom de l'artefact à downloader dans l'interface du pipeline 
     paths:
       - target/*
     reports:
       cobertura: $CI_PROJECT_NAME_code_coverage_report.xml
       codequality: $CI_PROJECT_NAME_codequality_report.xml


#Job de Test    
mvn-test-job:                           #Ce job est chainé au job mvn-build-job et ne se lance que si dernier termine avec succès
   extends: .mvn-test-job-generic
   when: on_success
   coverage: '/Code coverage \d+\.\d+/'
   artifacts:
     expire_in: 1 day
     when: always
     name: $CI_PROJECT_NAME-test-artifacts         #Nom de l'artefact à downloader dans l'interface du pipeline 
     paths:
       - target/jacoco.exec
       - target/surefire-reports/TEST-*.xml
       - target/surefire-reports/*.txt
       - ./*/target/surefire-reports/TEST-*.xml
       - ./*/target/surefire-reports/*.txt
     reports:
       junit: $CI_PROJECT_NAME_junit_report.xml
       cobertura: $CI_PROJECT_NAME_code_coverage_report.xml
       codequality: $CI_PROJECT_NAME_codequality_report.xml
   #dependencies: []                                 # On ne récupère pas les artéfacts issus du job de build


#Job de Déploiement
mvn-deploy-job:
  extends: .deploy-job-generic
  when: always
  artifacts:
     expire_in: 1 day
     when: always
     name: $CI_PROJECT_NAME-deploy-artifacts              #Nom de l'artefact à downloader dans l'interface du pipeline 
     paths:
       - target/*

#Job de notification en succès
notif-success:                          #Ce job est chainé au job mvn-test-job et ne se lance que si dernier termine avec succès
  extends: .notif-success
  when: on_success


#Job de notification en échec
notif-failed:                           #Ce job est chainé au job mvn-test-job et ne se lance que si dernier termine en erreur
  extends: .notif-failed
  when: on_failure
      
